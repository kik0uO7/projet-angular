import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';

import { WelcomeViewComponent } from './components/welcome-view/welcome-view.component';
import { RegisterViewComponent } from './components/register-view/register-view.component';
import { AuthentificationViewComponent } from './components/authentification-view/authentification-view.component';

import { RegisterService } from './services/register.service';
import { AuthentificateService } from './services/authentificate.service';
import { FiliereService } from './services/filiere.service';
import { CalculService } from './services/calcul.service';

import { UserGuardService } from './services/user-guard.service';
import { AdminGuardService } from './services/admin-guard.service';
import { ManageViewComponent } from './components/manage-view/manage-view.component';
import { ManageMatiereViewComponent } from './components/manage-matiere-view/manage-matiere-view.component';
import { CalculViewComponent } from './components/calcul-view/calcul-view.component';

const appRoutes: Routes = [
  { path: '', component: WelcomeViewComponent },
  { path: 'login', component: AuthentificationViewComponent },
  { path: 'register', component: RegisterViewComponent },
  { path: 'notes', canActivate: [UserGuardService], component: CalculViewComponent },
  { path: 'management', canActivate: [AdminGuardService], component: ManageViewComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    AuthentificationViewComponent,
    WelcomeViewComponent,
    RegisterViewComponent,
    ManageViewComponent,
    ManageMatiereViewComponent,
    CalculViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthentificateService,
    UserGuardService,
    AdminGuardService,
    RegisterService,
    FiliereService,
    CalculService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
