import { Matiere } from './matiere.model';

export class Note {
    constructor(
        public id: number, 
        public valeur: number, 
        public coefficient: number,
        public appartient: string,
        public matiere: Matiere,
        public mid: number,
        public fid: number,
        public exam: boolean
    ) {}
}
