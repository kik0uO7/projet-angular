import { Filiere } from './filiere.model';
import { Note } from './note.model';


export const NOTATION = {  
    PROJET: 'PROJET',
    MAX_EXAM: 'MAX_EXAM',
    MOYENNE: 'MOYENNE'
}


export class Matiere {

    constructor(
        public id: number, 
        public nom: string,
        public ects: number,
        public nbNotes: number,
        public notes: Note[],
        public typeNotation: string,
        public fid: number,
        public filiere: Filiere,
        public option: boolean
    ) {}
}
