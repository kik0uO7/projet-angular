import { Matiere } from './matiere.model';

export class Filiere {
    constructor(
        public id: number,
        public name: string, 
        public matieres: Matiere[]
    ) { }
}