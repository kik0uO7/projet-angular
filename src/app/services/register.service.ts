import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RegisterService {
  ip: string = "http://to-back.localhost";
  
  constructor(private http: HttpClient) { }
  
  register(
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    roleName: string
  ){
    return new Promise((resolve, reject) => {
      this.http
        .post(this.ip + "/api/register", {
          email: email,
          password: password,
          firstName: firstName,
          lastName: lastName,
          roleName: roleName
        })
        .toPromise()
        .then(() => {
          resolve();
        }, 
        () => {
          reject();  
        }
      );
    })
  }

}
