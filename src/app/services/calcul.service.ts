import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FiliereService } from './filiere.service';
import { Filiere } from '../models/filiere.model';
import { NOTATION } from '../models/matiere.model';

@Injectable()
export class CalculService {
    ip: string = "http://to-back.localhost"
    constructor(
        private cookieService: CookieService,
        private http: HttpClient,
        private filiereService: FiliereService
    ) { }

    saveNoteToService(note: Object) {
        return new Promise((resolve, reject) => {
            this.http.put<Filiere>(this.ip + "/api/notes/notes/add", note, {headers: {Authorization: "Bearer " + this.cookieService.get('token')}})
            .toPromise()
            .then(response => {
                this.filiereService.updateFiliere(response);
                resolve();
            }, (error) => {
                console.log("error")
                reject(error);
            });
        });
    }

    parseForm(form) {
        var notes: Object[] = [];
        var i = 0;
        var valeur: number = -1;
        var coeff: number = -1;
        var exam: boolean;
        var mid: string;
        var fid: string;
        for (var val in form) {
            i %= 3;
            var splitted: string[] = val.split('_');
            if (i === 0) {
                if (valeur !== -1) {
                    notes.push({
                        valeur: valeur,
                        coefficient: coeff,
                        appartient: this.cookieService.get('email'),
                        mid: mid,
                        fid: fid,
                        exam: exam
                    });
                }
                valeur = form[val];
            }
            else if (i === 1) {
                coeff = form[val];
            }
            else {
                exam = form[val];
            }
            mid = splitted[1];
            fid = splitted[0];
            i++;
        }

        notes.push({
            valeur: valeur,
            coefficient: coeff,
            appartient: this.cookieService.get('email'),
            mid: mid,
            fid: fid,
            exam: exam
        });
        for (var note of notes) {
            this.saveNoteToService(note).then(() => {}, () => {});
        }
        return notes
    }


    getMoyenneTotale(notes: Object[], idFiliere: number) : number {
        var mids : number[] = [];
        var ects: number[] = [];
        for (var note of notes) {
            if (!mids.includes(note['mid'])) {
                mids.push(note['mid']);
                ects.push(this.filiereService.getMatieresEctsById(note['fid'], note['mid']));
            }
        }


        var moyenne : number = 0;
        var ectsSum : number = 0;
        for (var i = 0; i < mids.length; i++) {
            moyenne += this.getMoyenne(notes, idFiliere, mids[i]) * ects[i];
            ectsSum += ects[i];
        }

        moyenne /= ectsSum;
        return moyenne;

    }

    getMoyenne(notes: Object[], idFiliere: number, idMatiere: number) {
        var noteByMid: any[] = [];
        var notationType = this.filiereService.getMatieresNotationById(idFiliere, idMatiere);
        for (var note of notes) {
            if (note['mid'] == idMatiere) {
                noteByMid.push(note);
            }
        }

        var moyenne: number = 0;
        var coeff: number = 0;
        var exam_val: number;
        if (notationType === NOTATION.MAX_EXAM) {
            for (var x of noteByMid) {
                moyenne += x.valeur * x.coefficient;
                coeff += x.coefficient;
                if (x.exam) {
                    exam_val = x.valeur;
                }
            }
            moyenne /= coeff;
            moyenne = (moyenne<exam_val)? exam_val : moyenne;
            
        }
        else if (notationType === NOTATION.MOYENNE) {
            for (var x of noteByMid) {
                moyenne += x.valeur * x.coefficient;
                coeff += x.coefficient;
                if (x.exam) {
                    exam_val = x.valeur;
                }
            }
            moyenne /= coeff;
            
        }
        else if (notationType === NOTATION.PROJET) {
            for (var x of noteByMid) {
                moyenne = x.valeur;
                
            }
        }
        
        
        return moyenne;
        
    }
    
}