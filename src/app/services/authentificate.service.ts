import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthentificateService {
  private ip: string = "http://to-back.localhost"
  
  constructor(private http: HttpClient,
    private cookies: CookieService) { }
  
  signInWithUsernameAndPassword(username: string, password: string) {
    return new Promise((resolve, reject) => {
      this.http.post(this.ip + "/api/login", {
        username : username,
        password : password
        })
        .toPromise()
        .then(resultat => {
          this.cookies.set('email', username);
          this.cookies.set('token', resultat["accessToken"])
          resolve();
        },
        () => reject());
    });

  }

  signOut() {
    return new Promise((resolve, reject) => {
      let fullToken: string = "Bearer " + this.cookies.get('token');
      this.http
        .post(this.ip + "/api/logout", {}, {headers: {Authorization: fullToken}})
        .toPromise()
        .then(() => {
          this.cookies.deleteAll();
          resolve();
        }, () => reject());
    });
  }
      
  verifyAdmin(email: string, token: string) {
    return new Promise<boolean>((resolve, reject) => {
      let fullToken: string = "Bearer " + token;
      this.http.
        post(this.ip + "/api/login/role", {email : email}, {headers: {Authorization: fullToken}})
        .toPromise()
        .then((response) => {
          if (response["roleName"] === "ROLE_ADMIN") {
            resolve(true);
          }
          else {
            reject();
          }
        }, () => reject());
      }
    );
  }

  verify(email: string, token: string) {
    return new Promise<boolean>((resolve, reject) => {
      let fullToken: string = "Bearer " + token;
      this.http.
        post(this.ip + "/api/login/role", {email : email}, {headers: {Authorization: fullToken}})
        .toPromise()
        .then((response) => {
          if (response["roleName"] === "ROLE_ADMIN" || response["roleName"] === "ROLE_USER") {
            resolve(true);
          }
          else {
            reject();
          }
        }, () => reject());
      }
    );
  }

}
