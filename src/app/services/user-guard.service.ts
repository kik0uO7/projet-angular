import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthentificateService } from './authentificate.service';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class UserGuardService implements CanActivate {
    constructor(private authService: AuthentificateService,
        private router: Router,
        private cookieService: CookieService) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (this.cookieService.check('email') && this.cookieService.check('token')) {
            return this.authService.verify(this.cookieService.get('email'), this.cookieService.get('token'));
        }
        else {
            this.router.navigate(['./login']);
        }
    }
}