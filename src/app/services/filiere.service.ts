import { Injectable } from '@angular/core';
import { Filiere } from '../models/filiere.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';


@Injectable()
export class FiliereService {
    private ip: string = "http://to-back.localhost/api/notes"

    private filieres: Filiere[] = [];
    private filiereSuject = new Subject<any[]>();
    
    constructor(
        private http: HttpClient,
        private cookie: CookieService
    ) {}

    getFilieresFromService() {
        this.filieres = [];
        return new Promise((resolve, reject) => {
            this.http
            .get<Filiere[]>(this.ip + "/filieres", {headers: {Authorization: "Bearer " + this.cookie.get('token'), "Content-type": "application/json"}})
            .toPromise()
            .then(response => {
                this.addAll(response);
                this.emitFiliereSubject();
                resolve();
            },
            () => reject());
        });      
    }

    addFilieresToService(name: string) {
        return new Promise((resolve, reject) => {
            this.http
            .put<Filiere>(this.ip + "/filieres/add", {nom: name}, {headers: {Authorization: "Bearer " + this.cookie.get('token'), "Content-type": "application/json"}})
            .toPromise()
            .then(value => {
                this.addFiliere(value);
                resolve(value);
            },
            () => reject());
        });
    }

    addMatiereToService(matiere: Object) {
        return new Promise((resolve, reject) => {
            this.http
            .put<Filiere>(this.ip + "/matieres/add", matiere, {headers: {Authorization: "Bearer " + this.cookie.get('token'), "Content-type": "application/json"}})
            .toPromise()
            .then(
            (value) => {
                this.getFilieresFromService();
                resolve(value);
            },
            () => reject());
        });
    }

    emitFiliereSubject() {
        this.filiereSuject.next(this.filieres.slice());
    }


    updateFiliere(filiere: Filiere) {
        for (var f of this.filieres) {
            if (f.id == filiere.id) {
                f = filiere;
                this.emitFiliereSubject();
            }
        }
    }

    addFiliere(filiere: Filiere) {
        this.filieres.push(filiere);
        this.emitFiliereSubject()
    }    


    addAll(f: Filiere[]) {
        f.forEach(value => {
            this.addFiliere(value)
        });    
    }    

    getFilieres() {
        return this.filieres;
    }

    getFiliereById(id: number) {
        for (var f of this.filieres) {
            if (id == f.id) {
                return f
            }
        }
    }
    
    getMatieres(id: number) {
        for (var f of this.filieres) {
            if (id == f.id) {
                return f.matieres
            }
        }
    }


    getMatieresNotationById(idFiliere: number, idMatiere: number) : string {
        for (var f of this.filieres) {
            if (idFiliere == f.id) {
                for (var m of f.matieres) {
                    if (idMatiere == m.id) {
                        return m.typeNotation;
                    }

                }
            }
        }
    }
    getMatieresEctsById(idFiliere: number, idMatiere: number) : number {
        for (var f of this.filieres) {
            if (idFiliere == f.id) {
                for (var m of f.matieres) {
                    if (idMatiere == m.id) {
                        return m.ects;
                    }

                }
            }
        }
        return 0;
    }
}