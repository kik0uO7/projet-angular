import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthentificateService } from 'src/app/services/authentificate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-authentification-view',
  templateUrl: './authentification-view.component.html',
  styleUrls: ['./authentification-view.component.scss']
})
export class AuthentificationViewComponent implements OnInit {
  failed: boolean = false;
  msg: string = "";

  constructor (private authService: AuthentificateService,
    private router: Router) { }

  ngOnInit(): void {}


  // Ne pas oublier d'add ngModel dans les champs qu'on veut récup 
  onSignIn(form: NgForm) {
    const username = form.value['username'];
    const password = form.value['password'];

    this.authService.signInWithUsernameAndPassword(username, password).then(
      () => this.router.navigate(['./']),
      () => this.failed = true
    );    
  }
}
