import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMatiereViewComponent } from './manage-matiere-view.component';

describe('ManageMatiereViewComponent', () => {
  let component: ManageMatiereViewComponent;
  let fixture: ComponentFixture<ManageMatiereViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageMatiereViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMatiereViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
