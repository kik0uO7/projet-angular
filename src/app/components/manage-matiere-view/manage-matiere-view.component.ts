import { Component, OnInit, Input, Injectable, OnDestroy } from '@angular/core';
import { FiliereService } from 'src/app/services/filiere.service';
import { Matiere, NOTATION } from 'src/app/models/matiere.model';
import { Observable, Subscription } from 'rxjs';
import 'rxjs/add/observable/interval';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-manage-matiere-view',
  templateUrl: './manage-matiere-view.component.html',
  styleUrls: ['./manage-matiere-view.component.scss']
})

@Injectable()
export class ManageMatiereViewComponent implements OnInit, OnDestroy {
  @Input() filiereId: number;
  matieres: Matiere[] = []
  filiereIdSub: Subscription;

  constructor(private filiereService: FiliereService) { }

  ngOnInit(): void {
    const counter = Observable.interval(1000);
    this.filiereIdSub = counter.subscribe(
      () => {
        this.matieres = this.filiereService.getMatieres(this.filiereId);
      }
    );

  }

  ngOnDestroy(): void {
    this.filiereIdSub.unsubscribe();
  }


  onAddMatiere(form: NgForm) {
    const nom: string = form.value['inputNom'];
    const ects: number = form.value['inputEcts'];
    const nbNotes: number = form.value['inputNbNotes'];
    const typeNotation: string = form.value['inputNotation'];
    const option: boolean = form.value['inputOption'];

    var data = {
      nom: nom,
      ects: ects,
      nbNotes: nbNotes,
      fid: this.filiereId,
      typeNotation: typeNotation,
      option: option
    };

    this.filiereService.addMatiereToService(data).then(() => {
      this.matieres = this.filiereService.getMatieres(this.filiereId);
      console.log(this.matieres)
    }, () => {});
  }

}
