import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegisterService } from 'src/app/services/register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.scss']
})
export class RegisterViewComponent implements OnInit {
  failed: boolean = false;

  constructor(private registerService: RegisterService, 
    private router: Router) { }

  ngOnInit(): void {
  }

  onRegister(form: NgForm) {
    const email = form.value['email'];
    const password = form.value['password'];
    const firstName = form.value['firstName'];
    const lastName = form.value['lastName'];
    const roleName = form.value['roleName'];

    this.registerService.register(email, password, firstName, lastName, roleName).then(
      () => this.router.navigate(['./login']),
      () => this.failed = true 
    );  
  }

}
