import { Component, OnInit } from '@angular/core';
import { Filiere } from 'src/app/models/filiere.model';
import { FiliereService } from 'src/app/services/filiere.service';
import { NgForm } from '@angular/forms';
import { CalculService } from 'src/app/services/calcul.service';

@Component({
  selector: 'app-calcul-view',
  templateUrl: './calcul-view.component.html',
  styleUrls: ['./calcul-view.component.scss']
})
export class CalculViewComponent implements OnInit {
  filieres: Filiere[] = [];
  notes: Object[] = [];
  selectedId: number = 101;
  calculOn: boolean = false;
  activeFiliere: Filiere;

  constructor(
    private filiereService: FiliereService,
    private calculService: CalculService
  ) { }

  ngOnInit(): void {
    this.filiereService.getFilieresFromService().then(() => {}, () => {});
    this.filieres = this.filiereService.getFilieres();
  }

  onClick(id: number) {
    this.selectedId = id;
    this.activeFiliere = this.filiereService.getFiliereById(id);
  }

  arrayNotes(val: number) {
    return Array(val).fill(0).map((x, i) => i);
  }

  onSubmit(form: NgForm) {
    this.notes = this.calculService.parseForm(form.value);
    this.calculOn = true;
    console.log(this.filiereService.getFilieres());
  }

  getNameForm(name: string, matiereId: string, nNote: string): string { 
    return this.selectedId + '_' + matiereId + '_' + nNote + '_' + name;
  }

  moyenneTotale() {
    return this.calculService.getMoyenneTotale(this.notes, this.selectedId);
  }
  
  moyenneMatiere(id: number) {
    return this.calculService.getMoyenne(this.notes, this.selectedId, id); 
  }
}
