import { Component, OnInit, OnDestroy } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthentificateService } from 'src/app/services/authentificate.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs';
import 'rxjs/add/observable/interval';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'note-angular'
  role: string = "";
  counterSubscription: Subscription;

  
  constructor(private cookies: CookieService,
    private authentificateService: AuthentificateService,
    private router: Router) { }

  onLogout() {
    this.role = ""
    this.authentificateService.signOut()
  }

  ngOnInit(): void {
    const counter = Observable.interval(2000);

    this.counterSubscription = counter.subscribe(
      () => {
        if (this.role === "") {
          if (this.cookies.check("token")) {
            this.authentificateService.verifyAdmin(this.cookies.get("email"), this.cookies.get("token")).then(
              () => this.role = "ROLE_ADMIN",
              () => this.authentificateService.verify(this.cookies.get("email"), this.cookies.get("token")).then(
                () => this.role = "ROLE_USER",
                () => {
                  this.cookies.deleteAll();
                }
              )

            );
          }
        }
      }
    );
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();

  }
}
