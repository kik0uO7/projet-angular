import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Filiere } from 'src/app/models/filiere.model';
import { FiliereService } from 'src/app/services/filiere.service';

@Component({
  selector: 'app-manage-view',
  templateUrl: './manage-view.component.html',
  styleUrls: ['./manage-view.component.scss']
})
export class ManageViewComponent implements OnInit {
  id: number = 101; 
  failed: boolean = false;

  filieres: Filiere[] = [];

  constructor(
    private filiereService: FiliereService
  ) {  }

  ngOnInit(): void {
    this.filiereService.getFilieresFromService().then(() => {}, () => {});
    this.filieres = this.filiereService.getFilieres();
  }
  onCreate(form: NgForm) {
    const name = form.value["inputFiliere"];
    this.filiereService.addFilieresToService(name).then(
      (value: Filiere) => this.id = value.id,
      () => this.failed = true
    );
  }
 
  onSelect(form: NgForm) {
    const id: number = form.value["inputSelect"]
    this.id = id;
  } 
}
